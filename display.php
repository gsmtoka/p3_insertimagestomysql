<!DOCTYPE html>
<html>
    <head>
        <title>Display MySQL Images</title>
        <script scr="https://code.jquery.com/jquery-2.1.3.min.js"></script>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" rel="stylesheet" 
                integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" 
                integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script> 
    </head>
    <body>
        <?php
        $mysqli = new mysqli('localhost', 'root', '', 'images') or die($mysqli->connect_error);
        $table = 'pets';

        $result = $mysqli->query("SELECT * FROM $table") or die($mysqli->error);

        while ($data = $result->fetch_assoc()){
            echo "<h2>{$data['name']}</h2>";
            echo "<img src='{$data['img_dir']}' width='20%' height='20%'>"; 
        }
        ?>

    </body> 

</html>